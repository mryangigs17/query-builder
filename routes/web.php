<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/data-tables', 'DataController@datatable');
Route::get('/table',function(){
    return view('halaman.table');
});




Route::get('/', 'IndexController@dashboard');
Route::get('/pendaftaran', 'FormController@pendaftaran');
Route::get('/kirim', 'FormController@welcome');


//CRUD Genre:
//mengarah ke form create data
Route::get('/cast/create', 'castController@create');
//menyimpan data ke table cast
Route::POST('/cast','castController@store');
//meanmpilkan list dari table cast
Route::get('/cast', 'castController@index');
//menampilkan detail pemain
Route::get('/cast/{cast_id}','castController@show');
//mengarah ke form edit data
Route::get('cast/{cast_id}/edit', 'castController@edit');
//meyimpan perubahan
Route::PUT('/cast/{cast_id}','castController@update');
//menghapus data
Route::DELETE('/cast/{cast_id}','castController@destroy');

