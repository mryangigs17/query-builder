<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function pendaftaran()
    {
        return view('halaman.form');
    }

    public function welcome(Request $request)
    {
       // dd($request->all());
       $namaD = $request['nama_depan'];
       $namaB = $request['nama_belakang'];

       return view('halaman.welcome', compact('namaD','namaB'));
    }
}

?>
