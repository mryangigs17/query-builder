@extends('layout.master')
@section('judul')
Halaman Form
@endsection

@section('content')
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
        <label for="exampleInputEmail1">Nama Cast</label>
        <input type="string" class="form-control" name="nama">
        </div>

        @error('nama')
        <div class=”alert alert-danger”>{{ $message }}
        </div>
        @enderror

        <div class="form-group">
            <label for="exampleInputEmail1">Umur</label>
            <input type="integer" class="form-control" name="umur">
            </div>
    
            @error('umur')
            <div class=”alert alert-danger”>{{ $message }}
            </div>
            @enderror


        <div class="form-group">
        <label for="exampleInputPassword1">Bio</label>
        <textarea name="bio" class="form-control"  placeholder="Biodata"></textarea>
        </div>
        @error('bio')
        <div class=”alert alert-danger”>{{ $message }}
        </div>
        @enderror



        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection