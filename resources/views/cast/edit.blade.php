@extends('layout.master')
@section('judul')
Halaman edit form
@endsection

@section('content')
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
        <label>Nama Cast</label>
        <input type="text" value="{{$cast->nama}}" class="form-control" name="nama">
        </div>

        @error('nama')
        <div class=”alert alert-danger”>{{ $message }}
        </div>
        @enderror

        <div class="form-group">
            <label for="exampleInputEmail1">Umur</label>
            <input type="integer" value="{{$cast->umur}}" class="form-control" name="umur">
            </div>
    
            @error('umur')
            <div class=”alert alert-danger”>{{ $message }}
            </div>
            @enderror


        <div class="form-group">
        <label for="exampleInputPassword1">Bio</label>
        <textarea name="bio" class="form-control"  placeholder="Biodata">{{$cast->bio}}</textarea>
        </div>
        @error('bio')
        <div class=”alert alert-danger”>{{ $message }}
        </div>
        @enderror



        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection