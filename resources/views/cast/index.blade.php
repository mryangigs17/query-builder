  @extends('layout.master')
  @section('judul')
  Halaman List Cast
  @endsection

  @section('content')
<a href="/cast/create" class="btn btn-primary my-3">Tambah Cast</a>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">nama</th>
        <th scope="col">umur</th>
        <th scope="col">bio</th>
        <th scope="col">action</th>
      </tr>
    </thead>
    <tbody>

      @forelse ($cast as $key => $item)
        <tr>
          <td>{{$key+1}}</td>
          <td>{{$item->nama}}</td>
          <td>{{$item->umur}}</td>
          <td>{{$item->bio}}</td>
          <td>
            <form action="/cast/{{$item->id}}" method="POST">
              @csrf
            <a href="/cast/{{$item->id}}" class='btn btn-info btn-sm'>Detail</a>
            <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">edit</a>
           
              @method('DELETE')
              <input type="submit" class="btn btn-danger btn-sm" value="DELETE">
            </form>
          </td>
         
      @empty
         <h1>Data Kosong</h1>
      @endforelse
    </tbody>
  </table>
  
 
  @endsection